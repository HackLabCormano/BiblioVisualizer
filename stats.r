#!/usr/bin/env Rscript

#library(ggplot2)
#library(pastecs)
library(lubridate)

dati = read.csv("CSBNO.csv")

dates <- ymd_hm(dati[['data']])

dates

#dates=as.Date(dati[['data']],format="%y%m%d%%H%M",origin="1970")

#dati$prestiti

summary(dati)
#stat.desc(dati)

jpeg("CSBNO.jpg",width=30,height=15,units="cm",res=200)
#plot(dati[['prestiti_oggi']]) #,xlim=as.Date(c("2018/01/01","2018/01/31")),type="h")

#17 12 27 0700
#plot(dati[['prestiti_oggi']],dati[['prestiti_oggi']],xlim=as.Date(c("2017/12/01","2018/02/15")))
#plot(dati[['data']],dati[['prestiti_oggi']],xlim=c(1800000000,1802000000))

#plot(dati[['prestiti_oggi']],type="h")
plot(dates,dati$prestiti_oggi,type="h")

#plot(dati[['prestiti']])

#lines(dati[['Timestamp']],dati[['Download']],type=opts[i])

#jpeg("$DAYS-2016.jpg")
#plot(dates,dati[['devices']],xlim=as.Date(c("2016/01/01","2016/12/31")),xlab="date",ylab="devices",type="h")
#axis(side=1,at=dates)
#axis(side=1,tck=.02)g
#plot(as.Date(dati[["date"]],format="%Y/%m/%d",origin="2016/01/01"),dati$devices)
