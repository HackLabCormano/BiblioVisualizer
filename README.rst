BiblioVisualizer
================

.. image:: img/2018-07-18_21.15.57.jpg
	:height: 200px

Un "gioco" di visualizzazione dei dati sul prestito libri nel Consorzio (CSBNO).

La barra (attualmente) mostra un "termometro" dei prestiti cumulativi della giornata in tutto il consorzio.

Parte da 0 (tutti i LED chiari) a mezzanotte di ogni giorno.
Durante il giorno sale fino a riempire la barra di rosso man mano che nel consorzio vengono prestati libri, ogni LED corrisponde a 100 prestiti.
Un cursore blu mostra il tempo che passa (si sposta ogni secondo) e un impulso colorato di giallo scatta ogni volta che la barra riesce a collegarsi in rete.

Scopi
~~~~~

-  rendere parzialmente interattivo e “giocoso” (*gamification*) il prestito libri
-  Coinvolgimento attivo degli utenti
-  “Termometro” biblioteca immediatamente visibile
-  Community: concorso per progettare visualizzazione

Origine dei dati
~~~~~~~~~~~~~~~~

Su http://webopac.csbno.net c’è un “cruscotto”, i dati (cumulativi e aggiornati ogni ora) provengono da http://frassino.comperio.it/kpi/clavis-csbno.json sotto forma di un JSON:

   ``{"dbName":"clavis-csbno", "utenti":"334518", "prestiti":"159497", "prestiti_oggi":"1520", "utentiplus":"7157", "utenti_attivi":"88484"}``

Quale dato visualizzare? Ci vuole un dato un po’ variabile...
Di cui calcolare/misurare un range di valori.
Abbiamo scelto “prestiti_oggi”, variabile da 0 (a inizio giornata) a circa 5000 (a fine giornata) con punte di 7000 (cfr. http://atrent.it/CSBNO/CSBNO.log)

La “barra” LED
~~~~~~~~~~~~~~

-  ESP8266 (simil-Arduino + wifi)
-  striscia da 70 LED RGB (**singolarmente** pilotabili)
-  un lampadario industriale di 120cm di altezza

Considerazioni
~~~~~~~~~~~~~~

Il dato NON è *realtime*, una vera *gamification* non è possibile al momento.

La visualizzazione è “banale” (stile *progress bar*), ma il codice è libero (GPL), siamo aperti (concorsi fra gli utenti delle biblioteche!?!) a suggerimenti per altre opzioni, esempi (quasi, in funzione dei dati che verranno resi disponibili) implementabili:

-  1 LED (o segmento di barra, vengono vendute a metro!)   corrisponde a 1 biblioteca, colore corrisponde ai prestiti
-  1 LED (o segmento) corrisponde a un orario, cioè visualizzazione cronologica
-  segnalazione acquisti/donazioni scatena variazione momentanea del pattern di LED
-  aggiungendo identificazione via RFID o qr-code:
      -  notifica novità interessanti per l’utente
      -  concorsi a premi
