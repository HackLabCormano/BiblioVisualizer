/**
 * visualizzatore (striscia LED RGB) prestiti biblio
 * - si connette a https://frassino.comperio.it/kpi/clavis-csbno.json
 * - prende i dati di stato
 * - ne realizza una visualizzazione "grafica" su striscia led
 *
 * librerie:
 * - JSON
 * - Wifi+webclient
 * - FastLED o simili
 *
 *
 *
 * TODO: visualizzare solo quelli che cambiano? (e rimanere sull'ultimo cambiato)
 * TODO: standardizzare nomi variabili e costanti (Klingon?)
 * TODO: ssid broadcast coi dati letti
 *

	Copyright (C) 2018 HackLabCormano (http://hacklabcormano.it)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*

 */

// BOARD: wemos D1 R1

// esp8266-d5f0fa.local (in BIBLIO)

// WiFi & NTP & OTA
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>
#include <ArduinoOTA.h>
#include <NTPClient.h>
#include <WiFiUdp.h>


#include <ArduinoJson.h>

// https://frassino.comperio.it/kpi/clavis-csbno.json

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

#define IMPULSE_DELAY 10

// per invertire il verso della barra (ora appesa al contrario, con lo 0 in alto)
#define INVERTED

//#define BRIANZA

#ifdef BRIANZA
#define LED_DATA_PIN 1
#define NUM_LEDS 9
#define LOWENERGY false
#else
#define LED_DATA_PIN 5
#define NUM_LEDS 70
#define LOWENERGY false
#endif

#define PIR_PIN D7
#define STEP 10
#define MICROSTEP 10
#define FLASHES 10
#define BAR_DELAY 100

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, LED_DATA_PIN, NEO_GRB + NEO_KHZ800);



// TODO colori da provare, ora ho messo valori a occhio
// https://en.wikipedia.org/wiki/RGB_color_model
const uint32_t C_UTENTI=strip.Color(10,10,100);
const uint32_t C_PRESTITI=strip.Color(0,100,100);
const uint32_t C_PRESTITI_OGGI=strip.Color(100,0,0);
const uint32_t C_UTENTI_PLUS=strip.Color(100,100,0);
const uint32_t C_UTENTI_ATTIVI=strip.Color(100,70,0);
const uint32_t colori[]= {C_UTENTI,C_PRESTITI,C_PRESTITI_OGGI,C_UTENTI_PLUS,C_UTENTI_ATTIVI};
const uint32_t vuoto=strip.Color(50,50,50);
const uint32_t white=strip.Color(100,100,100);
const uint32_t violaCormano=strip.Color(53,12,106);

// WiFi
WiFiClient wifiClient;
IPAddress gateway;
byte mac[6];

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP,+60*60); // UTC+1

String ssid = "hacklabcormano.it";
String password = "arduino2linux";
//#include "account.h"

#define WIFI_TENTATIVI 100
#define USE_GW "[gw]"

// MQTT
// la lib è PubSubClient (https://pubsubclient.knolleary.net/)
#include <PubSubClient.h>  // mqtt

#define TOPIC "BiblioVisualizer"
const String mqtt_server = "mqtt.hacklabcormano.it";

PubSubClient mqtt_client(wifiClient);
#define DELAY_MQTT 1000
long lastMsg = 0;
#define MSG_LEN 150
char msg[MSG_LEN];

/*
#define WEBPORT 8080
ESP8266WebServer httpServer(WEBPORT);
ESP8266HTTPUpdateServer httpUpdater;
*/






/**
 * Stats al gen18
 * 		utenti		prestiti	oggi		utentiplus	attivi		fine giornata(*)
 * AVE	328.880,87	159.816,93	1.953,23	6.791,56	89.365,77	4.345,25
 * MAX	330.377,00	162.185,00	6.501,00	6.981,00	89.567,00	6.501,00
 * MIN	327.492,00	157.354,00	    0,00	6.580,00	89.170,00	  463,00
 * STDEV	913,55	  1.276,37	1.999,22	117,90		    90,79	1.557,37
 *
 * (*) usiamo questa colonna come prestiti della giornata perché modella correttamente il minimo!!!
 *
 * come range usiamo (med-stdev,med+stdev)
 *
 */

/*
typedef struct dato {
    int corrente;
    int precedente;
    int massimo;
} Dato;
*/

#define NUMCAMPI 5
int precedenti[NUMCAMPI];
int correnti[NUMCAMPI];

unsigned long minimi[]=		{340000-10000,	163000-1450,	0,					7000-200,		90000-2000};
unsigned long massimi[]=	{340000+10000,	163000+1450,	7000,				7000+200,		90000+2000}; // 7000 così sono 10 led ogni mille prestiti
String nomicampi[]=			{"utenti",		"prestiti",		"prestiti_oggi",	"utentiplus",	"utenti_attivi"};

boolean changed(int campo) {
    if(precedenti[campo]==correnti[campo]) return false;
    return true;
}

void printAll() {
    for(int campo=0; campo<NUMCAMPI; campo++) {
        Serial.print(nomicampi[campo]);
        Serial.print(": ");

        Serial.print(minimi[campo]);
        Serial.print(" - ");
        Serial.print(correnti[campo]);
        Serial.print(" (prec: ");
        Serial.print(precedenti[campo]);
        Serial.print(") - ");
        //Serial.print(changed(campo));
        //Serial.print(") ");
        //Serial.print(" su ");
        Serial.println(massimi[campo]);
    }
}

void led_setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    //pinMode(PIR_PIN, INPUT);

#ifdef BRIANZA
    FastLED.addLeds<WS2812B, LED_DATA_PIN, GRB>(leds, NUM_LEDS);
    //FastLED.setBrightness(4);
#else
    //FastLED.addLeds<WS2812B, LED_DATA_PIN, BGR>(leds, NUM_LEDS);
    //FastLED.setBrightness(10);
#endif

    /*
    for(int i=0; i<NUM_LEDS; i++) {
        leds[i].b=0; //random(255);
    }
    */

    strip.begin();
    strip.setBrightness(50);
    strip.show(); // Initialize all pixels to 'off'

    Serial.println("NeoPixel initialized...");
}

uint16_t ledIndex(uint16_t n) {
#ifdef INVERTED
    return NUM_LEDS-n-1;
#else
    return n;
#endif
}

uint32_t getPixelColor(uint16_t n) {
    return     strip.getPixelColor(ledIndex(n));
}
void setPixelColor(uint16_t n, uint8_t r, uint8_t g, uint8_t b) {
    strip.setPixelColor(ledIndex(n),r,g,b);
}
void setPixelColor(uint16_t n, uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
    strip.setPixelColor(ledIndex(n),r,g,b,w);
}
void setPixelColor(uint16_t n, uint32_t c) {
    strip.setPixelColor(ledIndex(n),  c);
}

void impulseUp(uint32_t col) {
    for(int nr=0; nr<NUM_LEDS; nr++) {
        uint32_t prec=getPixelColor(nr);
        setPixelColor(nr,col);
        strip.show();
        delay(IMPULSE_DELAY);
        setPixelColor(nr,prec);
        strip.show();
    }
}

void impulseDown(uint32_t col) {
    for(int nr=NUM_LEDS-1; nr>=0; nr--) {
        uint32_t prec=getPixelColor(nr);
        setPixelColor(nr,col);
        strip.show();
        delay(IMPULSE_DELAY);
        setPixelColor(nr,prec);
        strip.show();
    }
}


/*
void setAll() {
    leds[0]=utenti;
    leds[1]=prestiti;
    leds[2]=prestiti_oggi;
    leds[3]=utentiplus;
   leds[4]=utenti_attivi;
}
*/

void bar(uint32_t col,int len) {
    bar(col,violaCormano,len);
}

void bar(uint32_t col,uint32_t other_col,int len) {
    for(int nr=0; nr<NUM_LEDS; nr++) {
        if(nr<=len) {
            setPixelColor(nr,col);
            strip.show();
            delay(STEP);
        } else {
            setPixelColor(nr,other_col);
            strip.show();
        }

        if(nr!=len && LOWENERGY) {
            setPixelColor(nr,vuoto);
            strip.show();
        }
    }
}

/*
void barFlash(uint32_t col,int len, int flashes) {
    bar(col,len);
    for(int c=0; c<flashes; c++) {
        setPixelColor(len,vuoto);
        strip.show();
        delay(MICROSTEP);

        setPixelColor(len,col);
        strip.show();
        delay(MICROSTEP);
    }
}
*/

void displayOne(int campo) {
    int mapped=map(correnti[campo],minimi[campo],massimi[campo],0,NUM_LEDS);

    Serial.print(campo);
    Serial.print(":");
    Serial.println(mapped);

    /*
    if(changed(campo))
    	barFlash(colori[campo],mapped,FLASHES);
    else
    */
    bar(colori[campo],mapped);

    //delay(BAR_DELAY);
}

void show() {
    displayOne(2); // campo 2, prestiti_oggi
}

void mqtt_callback(char* topic, byte* payload, unsigned int length) {
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    /*
    for (int i=0; i<length; i++) {
        Serial.print((char)payload[i]);
    }
    */
    Serial.println();

    strip.clear();

    if(payload[0]=='O')
        impulseUp(white);

    if(payload[0]=='I')
        impulseDown(white);

    show();
}

void mqtt_reconnect() {
    // Loop until we're reconnected
    if (!mqtt_client.connected()) {
        Serial.print("Attempting MQTT connection...");
        if (mqtt_client.connect("BiblioVisualizer")) {  // si connette al BROKER dichiarando "identità"
            Serial.println("connected");
            mqtt_subscribe();
        } else {
            Serial.print("failed, rc=");
            Serial.print(mqtt_client.state());
        }
    }
}

void mqtt_send(String topic,String payload) {
    if (WiFi.status() == WL_CONNECTED) {
        if (!mqtt_client.connected()) {
            mqtt_reconnect();
            Serial.println("reconnected...");
        }

        if (mqtt_client.connected()) {
            if (mqtt_client.publish(topic.c_str(), payload.c_str())) {
                Serial.println("MQTT published");
            } else {
                Serial.println("MQTT error (connection error or message too large)");
            }
            mqtt_subscribe();
        }
    }
}

void mqtt_subscribe() {
    Serial.print("subscribing: ");
    Serial.println(mqtt_client.subscribe("ContaPersone/in-out")); // NB non gestisce wildcard!!!
}

void mqtt_setup() {
    mqtt_client.setServer(mqtt_server.c_str(), 1883); // 1883 è porta default MQTT
    mqtt_client.setCallback(mqtt_callback);
    mqtt_reconnect();
    Serial.println("mqtt set");
}

boolean wifi_setup() {
    WiFi.mode(WIFI_STA);
    WiFi.hostname(TOPIC);

    // We start by connecting to a WiFi network
    Serial.print("=== Connecting to: ");
    Serial.println(ssid);

    WiFi.disconnect();
    WiFi.begin(ssid.c_str(), password.c_str());

    strip.clear();
    for (int i=0;
            (WiFi.status() != WL_CONNECTED) && (i < WIFI_TENTATIVI) && digitalRead(0)==HIGH;
            i++) {
        delay(100);
        Serial.print(".");
        bar(strip.Color(0,100,0),i);
    }

    if(WiFi.status() == WL_CONNECTED) {
        Serial.println();
        Serial.println("+++ WiFi connected!");

        Serial.print("IP: ");
        Serial.println(WiFi.localIP());

        Serial.print("GW: ");
        Serial.println(WiFi.gatewayIP());

        timeClient.begin();
        delay(500);
        timeClient.update();

        // Port defaults to 8266
        ArduinoOTA.setPort(8266);

        // Hostname defaults to esp8266-[ChipID]
        ArduinoOTA.setHostname(TOPIC);

        // No authentication by default
        ArduinoOTA.setPassword((const char *)"barra");

        ArduinoOTA.onStart([]() {
            Serial.println("OTA Start");
        });
        ArduinoOTA.onEnd([]() {
            Serial.println("OTA End");
        });
        ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            //Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100)));
            int level=ledIndex(NUM_LEDS*progress/total);
            if(level%10==0)
                bar(strip.Color(0,0,200),level);
        });
        ArduinoOTA.onError([](ota_error_t error) {
            Serial.printf("OTA Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
            else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
            else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
            else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
            else if (error == OTA_END_ERROR) Serial.println("End Failed");
        });

        ArduinoOTA.begin();
        Serial.println("+++ OTA Ready");

        /*
        MDNS.begin(TOPIC);
        httpUpdater.setup(&httpServer);
        httpServer.begin();
        MDNS.addService("http", "tcp", WEBPORT);
        Serial.println("+++ WebOTA Ready");
        */

        return true;
    } else {
        Serial.println("--- WiFi FAILED!");
        return false;
    }
}



#include <TaskScheduler.h>

Scheduler runner;

Task taskGetAndShow(2*TASK_MINUTE, TASK_FOREVER, []() {
    Serial.println("Connecting...");

    if(WiFi.status() != WL_CONNECTED) wifi_setup();

    // Connect to HTTP server
    wifiClient.setTimeout(5000);
    if (!wifiClient.connect("frassino.comperio.it", 80)) {
        Serial.println("Connection failed");
        return;
    }

    Serial.println("Connected!");

    // Send HTTP request
    wifiClient.println("GET /kpi/clavis-csbno.json HTTP/1.0");
    wifiClient.println("Connection: close");
    if (wifiClient.println() == 0) {
        Serial.println("Failed to send request");
        return;
    }

    // Check HTTP status
    char status[32] = {0};
    wifiClient.readBytesUntil('\r', status, sizeof(status));
    if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
        Serial.print("Unexpected response: ");
        Serial.println(status);
        return;
    }

    // Skip HTTP headers
    char endOfHeaders[] = "\r\n\r\n";
    if (!wifiClient.find(endOfHeaders)) {
        Serial.println("Invalid response");
        return;
    }

    const size_t bufferSize = JSON_OBJECT_SIZE(6) + 1000; //120;
    DynamicJsonDocument jsonBuffer(bufferSize);
    //TODO: non più compatibile con versione 6 della lib, quando esce dalla beta va cambiato, per ora tenere ArduinoJson alla versione 5.xxx
    //forse poi StaticJsonBuffer<500> jsonBuffer;

    //const char* json = "{\"dbName\":\"clavis-csbno\",\"utenti\":\"327980\",\"prestiti\":\"158367\",\"prestiti_oggi\":\"2010\",\"utentiplus\":\"6731\",\"utenti_attivi\":\"89353\"}";
    //JsonObject& root = jsonBuffer.parseObject(json);

    // Parse JSON object
    JsonObject& root = jsonBuffer.parseObject(wifiClient);

    if (!root.success()) {
        Serial.println("Parsing failed!");
        mqtt_send("biblioparsing","failed");
        return;
    }

    for(int campo=0; campo<NUMCAMPI; campo++) {
        precedenti[campo]=correnti[campo];
        unsigned long tmp=root[nomicampi[campo]];
        correnti[campo] = tmp;
    }

    // Disconnect
    wifiClient.stop();

    // print status
    printAll();

    show();
});

/** visualizzare qualcosa ogni secondo... */
int current=0;
Task taskSeconds(TASK_SECOND, TASK_FOREVER,[]() {
    /*
    Serial.print(current);
    Serial.print(" ");
    */

    uint32_t prev=getPixelColor(current);
    setPixelColor(current,strip.Color(0,0,250));
    strip.show();
    setPixelColor(current,prev);
    current--;
    if(current>=NUM_LEDS) current=0;
    if(current<0) current=NUM_LEDS-1;
});

/** per avere un feedback di funzionamento anche via mqtt */
Task taskMqtt(10*TASK_MINUTE, TASK_FOREVER, []() {
    Serial.print("heap: ");
    Serial.println(ESP.getFreeHeap());
    mqtt_send(TOPIC "/heap",String(ESP.getFreeHeap()));
    // TODO: mandare anche uno status sui dati biblio?
});

/** per buttar fuori qualcosa e vedere se va, controlla anche wifi */
Task taskStatus(20*TASK_MINUTE, TASK_FOREVER, []() {
    impulseUp(strip.Color(200,200,0)); // TODO yellow? portare in define

    if (WiFi.status() != WL_CONNECTED) {
        Serial.println("no wifi!");
        delay(1000);
        impulseDown(strip.Color(150,0,0)); // segnala reboot
        //delay(5000);
        ESP.reset();
    }

    // test
    timeClient.update();
    Serial.print("Time: ");
    Serial.println(timeClient.getFormattedTime());
    Serial.print("EpochTime: ");
    Serial.println(timeClient.getEpochTime());
});


void setup() {
    Serial.begin(115200);
    Serial.println("booting...");

    // vari setup
    led_setup();
    wifi_setup();
    mqtt_setup();

    delay(5000);

    // Tasks
    runner.init();

    runner.addTask(taskGetAndShow);
    taskGetAndShow.enable();

    runner.addTask(taskMqtt);
    taskMqtt.enable();

    runner.addTask(taskStatus);
    taskStatus.enable();

    runner.addTask(taskSeconds);
    taskSeconds.enable();

    Serial.println("booted!");

    for(int i=0; i<3; i++)
        impulseUp(strip.Color(15,70,100));
}

void loop() {
    runner.execute();
    ArduinoOTA.handle();
    //httpServer.handleClient(); // WebOTA
    if(!mqtt_client.loop()) mqtt_reconnect();
}
